@extends('layouts.default')

@section('content')
	@if (Session::has('error'))
		<span style="color:red">
			Ett fel inträffade: 
			{{ trans(Session::get('error')) }}
		</span>
	@endif
	 
	 <p>Skriv in din e-postadress och ditt nya lösenord</p>
	{{ Form::open(array('route' => array('postReset', $survey_id, $token))) }}
		<table>
			<tr>
				<td>{{ Form::label('email', 'E-mail:') }}</td>
				<td>{{ Form::text('email', Input::old('email')) }}</td>
			</tr>
			
			<tr>
				<td>{{ Form::label('password', 'Lösenord: ') }}</td>
				<td>{{ Form::password('password') }}</td>
			</tr>
	 
			<tr>
				<td>{{ Form::label('password_confirmation', 'Bekräfta lösenord: ') }}</td>
				<td>{{ Form::password('password_confirmation') }}</td>
			</tr>
	 
			{{ Form::hidden('token', $token) }}
			{{ Form::hidden('survey_id', $survey_id) }}
			
			<tr>
				<td>{{ Form::submit('Bekräfta') }}</td>
				<td></td>
			</tr>
		</table>
	{{ Form::close() }}
@stop