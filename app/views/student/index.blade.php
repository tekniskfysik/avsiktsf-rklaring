@extends('layouts.default')

@section('content')
	<p>Du är inloggad till </p>
	<h2>{{ $survey['courseName'] }}, {{ $survey['year'] }}, LP {{ $survey['LP'] }}</h2>
	<p>som <em>{{ Auth::user()->email }}</em></p>
	<br />
	<p>Frågor vid kursstart:
		@if($hasAnsweredPreForm)
			<span style="color:green">Besvarad</span>  {{str_repeat('&nbsp;', 1);}} {{ link_to_route('userPreAnswers', 'Dina svar') }}
		@else
			{{ link_to_route('preForm', 'Svara') }}
		@endif
	</p>
	<p>Frågor vid kursslut: 
		@if($hasAnsweredPostForm)
			<span style="color:green">Besvarad</span>
		@else
			{{ link_to_route('postForm', 'Svara') }}
		@endif
	</p>
	<br />
	<p>{{ HTML::link('logout', 'Logga ut') }}</p>
@stop