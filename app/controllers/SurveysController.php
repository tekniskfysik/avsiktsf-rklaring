<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class SurveysController extends BaseController
{
	public $restful = true;
	
	/*--------------------------------------------------------------------------
	| index
	|--------------------------------------------------------------------------
	| Returns the view with the index page.
	|--------------------------------------------------------------------------*/
	public function get_index()
	{
		return View::make('surveys.index')
				->with('title', 'Tillgängliga formulär')
				->with('surveys', Survey::all());
	}

	/*--------------------------------------------------------------------------
	| create
	|--------------------------------------------------------------------------
	| Posts a new survey into the surveys-table if the validation succeeds.
	|--------------------------------------------------------------------------*/
	public function post_create()
	{
		
		$validation = Survey::validate(Input::all());
		
		if($validation->fails())
		{
			return Redirect::back()
				->withErrors($validation)
				->withInput();
		}
		else
		{
			$sur = new Survey;
			$sur->courseName = Input::get('courseName');
			$sur->year = Input::get('year');
			$sur->LP = Input::get('LP');
			$sur->save();
			
//TODO
			//Fixa så att den här routen pekar till kursens egen infosida
			return Redirect::route('adminSurveyInfo', array('id'=>$sur->id))
					->with('message', 'Nytt formulär skapat!');
		}
	}
}
?>