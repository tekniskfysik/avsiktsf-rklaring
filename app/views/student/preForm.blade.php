@extends('layouts.default')

@section('content')

	<h2>Avsiktsförklaring i kursen</h2>
	<h3>{{ $survey['courseName'] }}, {{ $survey['year'] }}, LP {{ $survey['LP'] }}</h3>
	
	{{ Form::open(array('url'=>'student/preForm', 'method'=>'post')) }}

		<b>{{ Form::label('program', $q0['question']) }}</b>
		{{ Form::text('program') }} <br/>
		{{ '(Var vänlig ange hela programnamnet, t.ex. "Teknisk fysik")' }}<br/><br/>
		
		<b>{{ Form::label('q1', $q1['question']) }}</b>
		@foreach($a1 as $a)
			<br/>{{ Form::radio('q1', $a['code']) }}{{ $a['alternative'] }}
		@endforeach
		{{ Form::text('q1_text') }}<br/><br/>

		<b>{{ Form::label('q2', $q2['question']) }}</b>
		@foreach($a2 as $a)
			<br/>{{ Form::radio('q2', $a['code']) }}{{ $a['alternative'] }}
		@endforeach
		{{ Form::text('q2_text') }}<br/><br/>

		<b>{{ Form::label('q3', $q3['question']) }}<br/></b>
		{{ Form::textarea('q3', null, array('size'=>'40x5')) }}<br/><br/>
		
		<b>{{ Form::label('q4', $q4['question']) }}<br/></b>
		{{ Form::textarea('q4', null, array('size'=>'40x5')) }}<br/>
		
		{{ Form::hidden('survey_id', $survey['id']) }}
		{{ Form::hidden('user_id', $user_id) }}
		
		{{ Form::submit('Svara') }}

	{{ Form::close() }}
	
	<br/><br/>{{ link_to_route('studentHome', 'Avbryt') }}
@stop