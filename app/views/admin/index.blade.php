@extends('layouts.default')

@section('content')
	<h3>Välkommen</h3>
	<p>Du är inloggad som administratör</p>
	<p>{{ link_to_route('adminSurveyList', 'Lista på befintliga formulär') }} <br />
	{{ link_to_route('adminNewSurvey', 'Skapa nytt formulär') }} </p>
	{{ link_to_route('adminLogout', 'Logga ut') }}
@stop