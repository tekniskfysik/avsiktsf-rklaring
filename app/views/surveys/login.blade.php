@extends('layouts.default')

@section('content')
	<p>Logga in för att utvärdera</p>
	<h2>{{ $survey['courseName'] }}, LP {{ $survey['LP'] }}, {{ $survey['year'] }}</h2>
	
	{{ Form::open(array('route'=>array('doLogin', $survey->id), 'method'=>'post')) }}
	<table>
		<tr>
			<td>{{ Form::label('email', 'E-mail:') }}</td>
			<td>{{ Form::text('email', Input::old('email')) }}</td>
		</tr>
		<tr>
			<td>{{ Form::label('password', 'Lösenord:') }}</td>
			<td>{{ Form::password('password') }}</td>
		</tr>
		{{ Form::hidden('id', $survey['id']) }}
		<tr>
			<td>{{ Form::submit('Logga in') }}</td>
			<td></td>
		</tr>
	</table>
	{{ Form::close() }}
	<br />
	{{ link_to_route('register', 'Registrering', $parameters=array('id'=>$survey['id'])) }}
	<br />
	{{ link_to_route('remind', 'Jag har glömt mitt lösenord', $parameters=array('id'=>$survey['id'])) }}
@stop