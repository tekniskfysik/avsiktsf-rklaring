<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*Route::get('/', function()
{
	return View::make('hello');
});*/

//===== Routes that are unavailable if you are not logged in as an administrator =====
Route::group(array('before' => 'auth_admin'), function(){
	Route::get('/admin', array('as'=>'admin', 'uses'=>'AdminController@get_index'));
	Route::get('/admin/logout', array('as'=>'adminLogout', 'uses'=>'AdminController@doLogout'));
	Route::get('/admin/newSurvey', array('as'=>'adminNewSurvey', 'uses'=>'AdminController@get_newSurvey'));
	Route::post('/admin/newSurvey', array('as'=>'adminStoreSurvey', 'before'=>'csrf', 'uses'=>'SurveysController@post_create'));
	Route::get('/admin/surveyList', array('as'=>'adminSurveyList', 'uses'=>'AdminController@get_surveyList'));
	Route::get('/admin/survey/{id}', array('as'=>'adminSurveyInfo', 'uses'=>'AdminController@get_surveyInfo'));
	Route::get('/admin/delete/{id}', array('as'=>'adminDelete', 'uses'=>'AdminController@showDelete'));
	Route::delete('/admin/delete/{id}', array('as'=>'adminDoDelete', 'before'=>'csrf', 'uses'=>'AdminController@doDelete'));
	Route::get('/admin/statistics/{id}', array('as'=>'statistics', 'uses'=>'AdminController@get_statistics'));
	Route::get('/admin/exportExcel/{id}', array('as'=>'exportExcel', 'uses'=>'AdminController@exportExcel'));	
});

Route::get('/', array('uses'=>'SurveysController@get_index'));
Route::get('/admin/login', array('as'=>'adminLogin', 'uses'=>'AdminController@get_showLogin'));
Route::post('/admin/login', array('as'=>'adminDoLogin', 'before'=>'csrf', 'uses'=>'AdminController@post_doLogin'));
Route::get('/surveys', array('as'=>'surveyList', 'uses'=>'SurveysController@get_index'));
Route::get('/surveys/{id}', array('as'=>'login', 'uses'=>'UsersController@get_showLogin'));
Route::post('/surveys/{id}', array('as'=>'doLogin', 'uses'=>'UsersController@post_doLogin'));
Route::get('/surveys/{id}/register', array('as'=>'register', 'uses'=>'UsersController@get_showRegister'));
Route::post('/surveys/{id}/register', array('as'=>'doRegister', 'before'=>'csrf', 'uses'=>'UsersController@post_doRegister'));
Route::get('/surveys/{id}/remind', array('as'=>'remind', 'uses'=>'RemindersController@getRemind'));
Route::post('/surveys/{id}/remind', array('as'=>'postRemind', 'before'=>'csrf', 'uses'=>'RemindersController@postRemind'));
Route::get('password/reset/{survey_id}/{token}', array('as'=>'reset', 'uses'=>'RemindersController@getReset'));
Route::post('password/reset/{survey_id}/{token}', array('as'=>'postReset', 'uses'=>'RemindersController@postReset'));

//===== Routes that are unavailable if you are not logged in as a student =====
Route::group(array('before' => 'auth_student'), function(){
	Route::get('/student', array('as'=>'studentHome', 'uses'=>'StudentController@get_index'));
	Route::get('/logout', array('uses'=>'UsersController@doLogout'));
	Route::get('/student/preForm', array('as'=>'preForm', 'uses'=>'StudentController@get_preForm'));
	Route::post('/student/preForm', array('before'=>'csrf', 'uses'=>'StudentController@post_preForm'));
	Route::get('/student/userPreAnswers', array('as'=>'userPreAnswers', 'uses'=>'StudentController@get_preAnswers'));
	Route::get('/student/postForm', array('as'=>'postForm', 'uses'=>'StudentController@get_postForm'));
	Route::post('/student/postForm', array('before'=>'csrf', 'uses'=>'StudentController@post_postForm'));
});


?>