<?php

class Survey extends Eloquent
{
	protected $table = 'surveys';
	
	//Only these attributes are able to be filled
	protected $fillable = array('courseName', 'year', 'LP');
	
	//Rules for the validator
	public static $rules = array(
		'courseName' => 'required',
		'year' => 'required|digits:4'
	);
	public static $messages = array(
		'courseName.required' => 'Du måste ange ett kursnamn!',
		'year.required' => 'Du måste ange ett årtal!',
		'year.digits' => 'Årtalet måste bestå av fyra siffror!'
	);
	
	public static function validate($data)
	{
		return Validator::make($data, static::$rules, static::$messages);
	}
	
	public function preanswers()
	{
		return $this->hasMany('Preanswer');
	}
	
	public function postanswers()
	{
		return $this->hasMany('Postanswer');
	}
	
	public function users()
	{
		return $this->hasMany('User');
	}
}