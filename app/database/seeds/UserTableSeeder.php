<?php

class UserTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('users')->delete();
		User::create(array(
			'email' => 'admin',
			'survey_id' => 0,
			'role' => 'admin',
			'password' => Hash::make('nimda4321')
		));
		User::create(array(
			'email' => 'student1@student.umu.se',
			'survey_id' => 1,
			'role' => 'student',
			'password' => Hash::make('1234')
		));
		User::create(array(
			'email' => 'student2@student.umu.se',
			'survey_id' => 1,
			'role' => 'student',
			'password' => Hash::make('1234')
		));
		User::create(array(
			'email' => 'student3@student.umu.se',
			'survey_id' => 1,
			'role' => 'student',
			'password' => Hash::make('1234')
		));
		User::create(array(
			'email' => 'student4@student.umu.se',
			'survey_id' => 1,
			'role' => 'student',
			'password' => Hash::make('1234')
		));
		User::create(array(
			'email' => 'student5@student.umu.se',
			'survey_id' => 2,
			'role' => 'student',
			'password' => Hash::make('1234')
		));
		User::create(array(
			'email' => 'student6@student.umu.se',
			'survey_id' => 2,
			'role' => 'student',
			'password' => Hash::make('1234')
		));		
	}

}