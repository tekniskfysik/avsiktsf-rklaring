
<?php


/*
  Save a file called .env.local.php with the following content for the local setup
  Save a file called .env.production.php with the following content for the production setup
  */
 


return array(
	'DB_HOST' => '127.0.0.1',
	'DB_NAME' => 'dbname',
	'DB_USERNAME' => 'root',
	'DB_PASSWORD' => '',
	
	'MAIL_USERNAME' => 'xxxx',
	'MAIL_PASSWORD' => 'xxxx',
	'MAIL_DRIVER' => 'smtp'  // or 'log' for testing purposes
);