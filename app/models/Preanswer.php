<?php

class Preanswer extends Answer
{
	protected $table = 'preanswers';
	
	//Only these attributes are able to be filled
	protected $fillable = array('program',
								'q1',
								'q1_text',
								'q2',
								'q2_text',
								'q3',
								'q4');
								
	protected $guarded = array('id', 'user_id', 'survey_id');
	
	public static $rules = array(
		'program' => 'required',
		'q1' => 'required',
		'q2' => 'required'
	);
	
	public static $messages = array(
		'q1.required' => 'Du måste ange ett alternativ på fråga 1',
		'q2.required' => 'Du måste ange ett alternatvi på fråga 2',
		'program.required' => 'Du måste ange ditt program',
		'q1_text.required' => 'Var vänlig fyll i textfältet till fråga 1',
		'q2_text.required' => 'Var vänlig fyll i textfältet till fråga 2'
	);
	
	//If the user chooses "annat: ", i.e. A4 and A5, the text field is required.
	public static function validate($data)
	{
		$v = Validator::make($data, static::$rules, static::$messages);
		
		$v->sometimes('q1_text', 'required', function($data){
			return str_is('A5', $data->q1);
		});
		
		$v->sometimes('q2_text', 'required', function($data){
			return str_is('A4', $data->q2);
		});
		
		return $v;
	}
}

?>