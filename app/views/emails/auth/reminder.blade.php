<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Nytt lösenord, Avsiktsförklaring</h2>

		<div>
			För att skapa ett nytt lösenord, fyll i formuläret på länken: {{ URL::to('password/reset', array($user['survey_id'], $token)) }}.<br/>
			Länken kommer att upphöra om {{ Config::get('auth.reminder.expire', 60) }} minuter.
		</div>
	</body>
</html>
