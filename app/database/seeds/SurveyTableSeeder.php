<?php

class SurveyTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('surveys')->delete();
		Survey::create(array(
			'courseName' => 'Laserfysik',
			'year' => '2014',
			'LP' => 3
		));
		Survey::create(array(
			'courseName' => 'Datastrukturer och algoritmer',
			'year' => '2013',
			'LP' => 3
		));
	}
}
?>