<?php

class AlternativeTableSeeder extends Seeder
{
	public function run()
	{
		DB::table('alternatives')->delete();
		Alternative::create(array(
			'question_code' => 'q1',
			'code' => 'A1',
			'alternative' => 'Jag är inte intresserad av kursens innehåll och jag tänker inte lägga ner mer arbete än nödvändigt för att bli godkänd.'
		));
		Alternative::create(array(
			'question_code' => 'q1',
			'code' => 'A2',
			'alternative' => 'Jag vill lära mig det som krävs för att bli godkänd på kursen.'
		));
		Alternative::create(array(
			'question_code' => 'q1',
			'code' => 'A3',
			'alternative' => 'Jag vill lära mig mer än det som krävs för godkänt för att få ett högre betyg.'
		));
		Alternative::create(array(
			'question_code' => 'q1',
			'code' => 'A4',
			'alternative' => 'Jag vill lära mig mer än det som krävs för godkänt eftersom jag vill lära mig mer om ämnet och få en djupare förståelse för det kursen behandlar.'
		));
		Alternative::create(array(
			'question_code' => 'q1',
			'code' => 'A5',
			'alternative' => 'Annat: '
		));
		Alternative::create(array(
			'question_code' => 'q2',
			'code' => 'A1',
			'alternative' => 'Mitt mål är att få betyget 3 på kursen.'
		));
		Alternative::create(array(
			'question_code' => 'q2',
			'code' => 'A2',
			'alternative' => 'Mitt mål är att få betyget 4 på kursen.'
		));
		Alternative::create(array(
			'question_code' => 'q2',
			'code' => 'A3',
			'alternative' => 'Mitt mål är att få betyget 5 på kursen.'
		));
		Alternative::create(array(
			'question_code' => 'q2',
			'code' => 'A4',
			'alternative' => 'Annat: '
		));
		Alternative::create(array(
			'question_code' => 'q5',
			'code' => 'A1',
			'alternative' => 'Mina egna mål och min avsikt för kursen har förändrats under kursens gång.'
		));
		Alternative::create(array(
			'question_code' => 'q5',
			'code' => 'A2',
			'alternative' => 'Jag har samma mål och avsikt för kursen nu, som vid kursstart.'
		));
		Alternative::create(array(
			'question_code' => 'q5',
			'code' => 'A3',
			'alternative' => 'Annat: '
		));
		Alternative::create(array(
			'question_code' => 'q6',
			'code' => 'A1',
			'alternative' => 'Jag upplever att jag uppnått mina egna inlärningsmål.'
		));
		Alternative::create(array(
			'question_code' => 'q6',
			'code' => 'A2',
			'alternative' => 'Jag upplever att jag inte har uppnått mina egna inlärningsmål.'
		));
		Alternative::create(array(
			'question_code' => 'q6',
			'code' => 'A3',
			'alternative' => 'Annat: '
		));
	}
}
?>