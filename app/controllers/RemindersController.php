<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class RemindersController extends Controller {

	/**
	 * Display the password reminder view.
	 *
	 * @return Response
	 */
	public function getRemind($id)
	{
		try {
			$survey = Survey::findOrFail($id);
		}
		catch(ModelNotFoundException $e) {
			//App::abort(404);
			return Response::make('Not Found', 404);
		}
		
		return View::make('password.remind')
				->with('title', 'Påminnelse')
				->with('survey', $survey);
	}	
	
	/**
	 * Handle a POST request to remind a user of their password.
	 *
	 * @return Response
	 */
	public function postRemind()
	{
		$credentials = array('email'=>Input::get('email'),
									'survey_id'=>Input::get('survey_id'));
									
		switch ($response = Password::remind($credentials,
					function($message){$message->subject('Nytt lösenord');})
		)
		{
			case Password::INVALID_USER:
				return Redirect::back()
						->with('error', Lang::get($response))
						->withInput();

			case Password::REMINDER_SENT:
				return Redirect::back()->with('success', Lang::get($response));
		}
	}

	/**
	 * Display the password reset view for the given token.
	 *
	 * @param  string  $token
	 * @return Response
	 */
	public function getReset($survey_id, $token = null)
	{
		if (is_null($token)) App::abort(404);

		return View::make('password.reset')
					->with('token', $token)
					->with('survey_id', $survey_id)
					->with('title', 'Nytt lösenord');
	}

	/**
	 * Handle a POST request to reset a user's password.
	 *
	 * @return Response
	 */
	public function postReset()
	{
		$credentials = Input::only(
			'email', 'password', 'password_confirmation', 'token', 'survey_id'
		);
		
		Password::validator(function($credentials){
			return strlen($credentials['password']) >= 1;
		});
		
		$response = Password::reset($credentials, function($user, $password)
		{
			$user->password = Hash::make($password);

			$user->save();
		});

		switch ($response)
		{
			case Password::INVALID_PASSWORD:
			case Password::INVALID_TOKEN:
			case Password::INVALID_USER:
				return Redirect::back()->with('error', Lang::get($response));

			case Password::PASSWORD_RESET:
				return Redirect::route('login', array('id' => Input::get('survey_id')))
							->with('message', 'Ditt lösenord är utbytt');;
		}
	}

}
