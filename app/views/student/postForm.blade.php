@extends('layouts.default')

@section('content')
	<h2>Uppföljning till avsiktsförklaring i kursen</h2>
	<h3>{{ $survey['courseName'] }}, {{ $survey['year'] }}, LP {{ $survey['LP'] }}</h3>
	
	{{ Form::open(array('url'=>'student/postForm', 'method'=>'post')) }}
	
		<b>{{ Form::label('q5', $q5['question']) }}</b>
		@foreach($a5 as $a)
			<br/>{{ Form::radio('q5', $a['code']) }}{{ $a['alternative'] }}
		@endforeach
		{{ Form::text('q5_text') }}<br/><br/>

		<b>{{ Form::label('q6', $q6['question']) }}</b>
		@foreach($a6 as $a)
			<br/>{{ Form::radio('q6', $a['code']) }}{{ $a['alternative'] }}
		@endforeach
		{{ Form::text('q6_text') }}<br/><br/>

		<b>{{ Form::label('q7', $q7['question']) }}<br/></b>
		{{ Form::textarea('q7', null, array('size'=>'40x5')) }}<br/><br/>
		
		<b>{{ Form::label('q8', $q8['question']) }}<br/></b>
		{{ Form::textarea('q8', null, array('size'=>'40x5')) }}<br/><br/>
		
		<b>{{ Form::label('q9', $q9['question']) }}<br/></b>
		{{ Form::textarea('q9', null, array('size'=>'40x5')) }}<br/><br/>
		
		<b>{{ Form::label('q10', $q10['question']) }}<br/></b>
		{{ Form::textarea('q10', null, array('size'=>'40x5')) }}<br/><br/>
		
		<b>{{ Form::label('q11', $q11['question']) }}<br/></b>
		{{ Form::textarea('q11', null, array('size'=>'40x5')) }}<br/><br/>
		
		{{ Form::hidden('survey_id', $survey['id']) }}
		{{ Form::hidden('user_id', $user_id) }}
		
		{{ Form::submit('Svara') }}
	
	{{ Form::close() }}
	
	<br />{{ link_to_route('studentHome', 'Avbryt') }}
@stop