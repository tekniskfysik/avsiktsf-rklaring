@extends('layouts.default')

@section('content')

	<h2>Så här svarade du i början av kursen</h2>
	<h3>{{ $survey['courseName'] }}, {{ $survey['year'] }}, LP {{ $survey['LP'] }}</h3>

	<td><b>Kryssa för det alternativ som passar bäst</b></td><br>
	@if ($preanswer->q1 === "A1")
		Jag är inte intresserad av kursens innehåll och jag tänker inte lägga ner mer arbete än nödvändigt för att bli godkänd.
	@elseif ($preanswer->q1 === "A2")
		Jag vill lära mig det som krävs för att bli godkänd på kursen.
	@elseif ($preanswer->q1 === "A3")
		Jag vill lära mig mer än det som krävs för godkänt för att få ett högre betyg.
	@elseif ($preanswer->q1 === "A4")
		Jag vill lära mig mer än det som krävs för godkänt eftersom jag vill lära mig mer om ämnet och få en djupare förståelse för det kursen behandlar.
	@elseif ($preanswer->q1 === "A5")
		Annat: 
		{{$preanswer->q1_text}}
	@endif <br><br>

	<td><b>Kryssa för det alternativ som passar bäst</b></td><br>
	@if ($preanswer->q2 === "A1")
		Mitt mål är att få betyget 3 på kursen.
	@elseif ($preanswer->q2 === "A2")
		Mitt mål är att få betyget 4 på kursen.
	@elseif ($preanswer->q2 === "A3")
		Mitt mål är att få betyget 5 på kursen.
	@elseif ($preanswer->q2 === "A4")
		Annat: 
		{{$preanswer->q2_text}}
	@endif <br><br>

	<td><b>Beskriv kortfattat varför du läser denna kurs: </b></td><br>

	{{$preanswer->q3}} <br><br>

	<td><b>Beskriv kortfattat vad du, till din kommande yrkessroll, vill lära dig av denna kurs: </b></td><br>

	{{$preanswer->q4}} <br><br>




	<br />{{ link_to_route('studentHome', 'Gå tillbaka') }}

@stop