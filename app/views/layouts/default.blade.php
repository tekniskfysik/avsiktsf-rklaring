<!DOCTYPE html>
<html>

<head>
	<title>{{ $title }}</title>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/stylesheet.css') }}" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
	<div class="header">
		<img src="{{ URL::asset('pictures/logga_transparent2.png') }}" alt="Teknisk fysik"/>
	</div>
	<div class="container">
		@if(Session::get('message'))
			<p style="color:red">{{ Session::get('message') }}</p>
		@endif
		
		@if($errors->has())
			<ul>
				@foreach ($errors->all('<li>:message</li>') as $error)
					<span style="color:red">{{ $error }}</span>
				@endforeach
			</ul>
		@endif
		
		@yield('content')
	</div>
</body>

</html>