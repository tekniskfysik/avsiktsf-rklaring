@extends('layouts.default')

@section('content')
	<h3>Tillgängliga formulär</h3>
	<table>
	@foreach($surveys as $survey)
		<tr>
			<td>{{ link_to_route('adminSurveyInfo',
					"$survey->courseName, $survey->year, LP $survey->LP",
					$parameters=array($survey->id)) }}</td>			
		</tr>
	@endforeach
	</table>
	<br />
	{{ link_to_route('admin', 'Tillbaka') }}
@stop