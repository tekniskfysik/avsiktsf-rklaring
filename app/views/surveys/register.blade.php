@extends('layouts.default')

@section('content')

	<p>Registrera dig till utvärdering av</p>
	<h2>{{ $survey->courseName }}, LP {{ $survey->LP }}, {{ $survey->year }} </h2>
	<p>Notera att din användare kommer raderas vid kursslut.</p>

	{{ Form::open(array('route'=>array('doRegister', $survey->id), 'method'=>'post')) }}
	<table>
		<tr>
			<td>{{ Form::label('email', 'E-mail:') }}</td>
			<td>{{ Form::text('email', Input::old('email')) }}</td>
		</tr>
		<tr>
			<td>{{ Form::label('password', 'Lösenord:') }}</td>
			<td>{{ Form::password('password') }}</td>
		</tr>
		<tr>
			<td>{{ Form::label('password_confirmation', 'Upprepa lösenord:') }}</td>
			<td>{{ Form::password('password_confirmation') }}</td>
		</tr>
		{{ Form::hidden('survey_id', $survey['id']) }}
		{{ Form::hidden('role', 'student') }}
		<tr>
			<td>{{ Form::submit('Registrera') }}</td>
			<td></td>
		</tr>
	</table>
	{{ Form::close() }}
	
	<br />
	{{ link_to_route('login', 'Tillbaka', $parameters=array('id'=>$survey->id)) }}
	
@stop