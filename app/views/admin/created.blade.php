@extends('layouts.default')

@section('content')
	<p>Nytt formulär är skapat till kursen:</p>
	<p>{{ $survey['courseName'] }}</p>
	<p>År: {{ $survey['year'] }}</p>
	<p>LP: {{ $survey['LP'] }}</p>
	<p>Länken nedan skickar dig till formuläret, kopiera och skicka ut till studenterna...</p>
	<p>{{ route('login', $params=array('id'=>$survey['id'])) }}</p>
	<p>{{ link_to_route('admin', 'Tillbaka') }}</p>
@stop()