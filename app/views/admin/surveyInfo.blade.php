@extends('layouts.default')

@section('content')
	<h3>{{ $survey->courseName }}, LP {{ $survey->LP }}, {{ $survey->year }}</h3>
	
	<p>Skicka ut följande länk till studenterna för registrering och inloggning
	{{ link_to_route('login', URL::route('login', array('id'=>$survey->id)), array('id'=>$survey->id)) }}</p>
	
	<p>{{ $numPre }} @if ($numPre == 1) student @else studenter @endif har svarat på enkät ett.</p>
	<p>{{ $numPost }} @if ($numPost == 1) student @else studenter @endif har svarat på enkät två.</p>
	
	<p>Ytterligare statistik [{{ link_to_route('statistics', 'statistics', $parameters=array($survey->id)) }}]</p><br/>
	{{ link_to_route('adminDelete', 'Radera enkät', $parameters=array($survey->id)) }}<br/>
	<br />
	{{ link_to_route('adminSurveyList', 'Lista på formulär') }} | 
	{{ link_to_route('admin', 'Tillbaka till startsidan') }}
@stop