@extends('layouts.default')

@section('content')
	<p>Logga in som administratör</p>
	
	{{ Form::open(array('route'=>'adminDoLogin', 'method'=>'post')) }}
	<table>
		<tr>
			<td>{{ Form::label('user', 'Användarnamn:') }}</td>
			<td>{{ Form::text('user', Input::old('email')) }}</td>
		</tr>
		<tr>
			<td>{{ Form::label('password', 'Lösenord:') }}</td>
			<td>{{ Form::password('password') }}</td>
		</tr>
		{{ Form::hidden('role', 'admin') }}
		<tr>
			<td>{{ Form::submit('Logga in') }}</td>
			<td></td>
		</tr>
	</table>
	{{ Form::close() }}
@stop