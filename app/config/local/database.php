<?php
return array(
'profile' => true,
'fetch' => PDO::FETCH_CLASS,
'migrations' => 'migrations',
'default' => 'mysql',
'connections' => array(
	'sqlite' => array(
		'driver' => 'sqlite',
		'database' => 'application',
		'prefix' => '',
		),
	
	'mysql' => array(
			'driver'    => 'mysql',
			'host'      => $_ENV['DB_HOST'],
			'database'  => $_ENV['DB_NAME'],
			'username'  => $_ENV['DB_USERNAME'],
			'password'  => $_ENV['DB_PASSWORD'],
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => 'avsikt_',
		),
	),
);
