<?php

class QuestionTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('questions')->delete();
		Question::create(array(
			'question' => 'Jag läser program: ',
			'code' => 'program'
		));
		Question::create(array(
			'question' => 'Kryssa för det alternativ som passar bäst',
			'code' => 'q1'
		));
		Question::create(array(
			'question' => 'Kryssa för det alternativ som passar bäst',
			'code' => 'q2'
		));
		Question::create(array(
			'question' => 'Beskriv kortfattat varför du läser denna kurs: ',
			'code' => 'q3'
		));
		Question::create(array(
			'question' => 'Beskriv kortfattat vad du, till din kommande yrkessroll, vill lära dig av denna kurs: ',
			'code' => 'q4'
		));
		Question::create(array(
			'question' => 'Kryssa för det alternativ som passar bäst',
			'code' => 'q5'
		));
		Question::create(array(
			'question' => 'Kryssa för det alternativ som passar bäst',
			'code' => 'q6'
		));
		Question::create(array(
			'question' => 'Om dina mål och din avsikt förändrats, beskriv kortfattat hur de förändrats och varför',
			'code' => 'q7'
		));
		Question::create(array(
			'question' => 'Om du inte har uppnått dina egna inlärningsmål, beskriv kortfattat varför',
			'code' => 'q8'
		));
		Question::create(array(
			'question' => 'Beskriv kortfattat nedan, vad du tar med dig från denna kurs till din kommande yrkesroll',
			'code' => 'q9'
		));
		Question::create(array(
			'question' => 'Beskriv kortfattat nedan, vad du tar med dig från denna kurs till din fortsatta utbildning',
			'code' => 'q10'
		));
		Question::create(array(
			'question' => 'Vilka förändringar av denna kurs skulle fått dina egna inlärningsmål att höjas? Beskriv kortfattat nedan',
			'code' => 'q11'
		));
	}
}
?>