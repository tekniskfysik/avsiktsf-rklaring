<?php

class AdminController extends BaseController
{
	/*--------------------------------------------------------------------------
	| index
	|--------------------------------------------------------------------------
	| Returns the index page for the administrator.
	|--------------------------------------------------------------------------*/
	public function get_index()
	{
		return View::make('admin.index')
				->with('title', 'Indexsida');
	}
	
	/*--------------------------------------------------------------------------
	| showLogin
	|--------------------------------------------------------------------------
	| Returns the login page for the administrator.
	|--------------------------------------------------------------------------*/
	public function get_showLogin()
	{
		return View::make('admin.adminLogin')
				->with('title', 'Logga in');
	}
	
	/*--------------------------------------------------------------------------
	| doLogin
	|--------------------------------------------------------------------------
	| Perform the authentication of the admin and logs him or her in.
	|--------------------------------------------------------------------------*/	
	public function post_doLogin()
	{
		$input = Input::all();
		
		$attempt = Auth::attempt(array(
					'email' => $input['user'],
					'password' => $input['password'],
					'role' => $input['role']
		));
		
		if($attempt)
		{
			return Redirect::route('admin');
		}
		else 
		{
			return Redirect::back()
					->withInput()
					->with('message', 'Felaktiga inloggningsuppgifter');
		}
	}

	/*--------------------------------------------------------------------------
	| newSurvey
	|--------------------------------------------------------------------------
	| Return a page with a form used to create new surveys.
	|--------------------------------------------------------------------------*/	
	public function get_newSurvey()
	{
		return View::make('admin.newSurvey')
				->with('title', 'Skapa nytt formulär');
	}	

	/*--------------------------------------------------------------------------
	| surveyList
	|--------------------------------------------------------------------------
	| Returns a list of available surveys.
	|--------------------------------------------------------------------------*/	
	public function get_surveyList()
	{
		return View::make('admin.surveyList')
				->with('title', 'Tillgängliga formulär')
				->with('surveys', Survey::all());
	}
	
	
	/*--------------------------------------------------------------------------
	| surveyInfo
	|--------------------------------------------------------------------------
	| Returns a view with information about a survey.
	|--------------------------------------------------------------------------*/	
	public function get_surveyInfo($id)
	{
		try {
			$survey = Survey::findOrFail($id);					
		}
		catch(ModelNotFoundException $e) {
			return Response::make('Not Found', 404);
		}
		
		$numPre = Survey::find($id)->preanswers->count();
		$numPost = Survey::find($id)->postanswers->count();
		
		return View::make('admin.surveyInfo')
				->with('title', $survey->courseName)
				->with('survey', $survey)
				->with('numPre', $numPre)
				->with('numPost', $numPost);
	}
	
	
	/*--------------------------------------------------------------------------
	| showDelete
	|--------------------------------------------------------------------------
	| Verifies that the administrator actually want to delete the survey.
	|--------------------------------------------------------------------------*/	
	public function showDelete($id)
	{
		try {
			$survey = Survey::findOrFail($id);					
		}
		catch(ModelNotFoundException $e) {
			return Response::make('Not Found', 404);
		}
		
		return View::make('admin.delete')
				->with('title', 'Radera formulär')
				->with('survey', $survey);
	}

	/*--------------------------------------------------------------------------
	| doDelete
	|--------------------------------------------------------------------------
	| Completely deletes a survey, its users and their answers.
	|--------------------------------------------------------------------------*/	
	public function doDelete()
	{
		$id = Input::get('survey_id');
		
		Preanswer::where('survey_id', $id)->delete();
		Postanswer::where('survey_id', $id)->delete();
		User::where('survey_id', $id)->delete();
		Survey::find($id)->delete();
		
		return Redirect::route('adminSurveyList')
				->with('message', 'formuläret är raderat');
	}	
	
	/*--------------------------------------------------------------------------
	| statistics
	|--------------------------------------------------------------------------
	| get statistics for a survey
	|--------------------------------------------------------------------------*/	
	public function get_statistics($id)
	{
		try {
			$survey = Survey::findOrFail($id);					
		}
		catch(ModelNotFoundException $e) {
			return Response::make('Not Found', 404);
		}

		try {
			$preanswers = Survey::find($id)->preanswers;
		}
		catch(ModelNotFoundException $e) {
			return Response::make('Not Found', 404);
		}

		try {
			$postanswers = Survey::find($id)->postanswers;
		}
		catch(ModelNotFoundException $e) {
			return Response::make('Not Found', 404);
		}

		

		

		/*try {
			$preanswer = Preanswer::findOrFail($id);
		}
		catch(ModelNotFoundException $e) {
			return Respone::make('Not found', 404);
		}*/
		

		return View::make('admin.statistics')
				->with('title', 'Statistics')
				->with('survey', $survey)
				->with('preanswers', $preanswers)
				->with('postanswers', $postanswers);
				//->with('preanswer', $preanswer);
	}

	public function exportExcel($id)
	{

		$q1a1 = 0;
		$q1a2 = 0;
		$q1a3 = 0;
		$q1a4 = 0;
		$q1a5 = 0;
		$q2a1 = 0;
		$q2a2 = 0;
		$q2a3 = 0;
		$q2a4 = 0;
		$q5a1 = 0;
		$q5a2 = 0;
		$q5a3 = 0;
		$q6a1 = 0;
		$q6a2 = 0;
		$q6a3 = 0;

		$totalpre = 0;
		$totalpost = 0;

		$textq1 = array();
		$textq2 = array();
		$textq3 = array();
		$textq4 = array();
		$textq5 = array();
		$textq6 = array();
		$textq7 = array();
		$textq8 = array();
		$textq9 = array();
		$textq10 = array();
		$textq11 = array();

		array_push($textq1, 'Fråga 1. Kryssa för det alternativ som passar bäst, annat');
		array_push($textq2, 'Fråga 2. Kryssa för det alternativ som passar bäst, annat');
		array_push($textq3, 'Beskriv kortfattat varför du läser denna kurs');
		array_push($textq4, 'Beskriv kortfattat vad du, till din kommande yrkessroll, vill lära dig av denna kurs');
		array_push($textq5, 'Uppföljande fråga 1. Kryssa för det alternativ som passar bäst, annat');
		array_push($textq6, 'Uppföljande fråga 2. Kryssa för det alternativ som passar bäst, annat');
		array_push($textq7, 'Om dina mål och din avsikt förändrats, beskriv kortfattat hur de förändrats och varför');
		array_push($textq8, 'Om du inte har uppnått dina egna inlärningsmål, beskriv kortfattat varför');
		array_push($textq9, 'Beskriv kortfattat vad du tar med dig från denna kurs till din kommande yrkesroll');
		array_push($textq10, 'Beskriv kortfattat vad du tar med dig från denna kurs till din fortsatta utbildning');
		array_push($textq11, 'Vilka förändringar av denna kurs skulle fått dina egna inlärningsmål att höjas?');
		

		$what_program = array();

	


		try {
			$survey = Survey::findOrFail($id);					
		}
		catch(ModelNotFoundException $e) {
			return Response::make('Not Found', 404);
		}

		try {
			$preanswers = Survey::find($id)->preanswers;
		}
		catch(ModelNotFoundException $e) {
			return Response::make('Not Found', 404);
		}

		try {
			$postanswers = Survey::find($id)->postanswers;
		}
		catch(ModelNotFoundException $e) {
			return Response::make('Not Found', 404);
		}

		foreach ($preanswers as $preanswer) {
			if ($preanswer->q1 == "A1") {
				$q1a1 += 1;
			}
			else if ($preanswer->q1 == "A2") {
				$q1a2 +=1;
			}
			else if ($preanswer->q1 == "A3") {
				$q1a3 +=1;
			}
			else if ($preanswer->q1 == "A4") {
				$q1a4 +=1;
			}
			else if ($preanswer->q1 == "A5") {
				$q1a5 +=1;
				array_push($textq1, $preanswer->q1_text);
			}


			if ($preanswer->q2 == "A1") {
				$q2a1 += 1;
			}
			else if ($preanswer->q2 == "A2") {
				$q2a2 +=1;
			}
			else if ($preanswer->q2 == "A3") {
				$q2a3 +=1;
			}
			else if ($preanswer->q2 == "A4") {
				$q2a4 +=1;
				array_push($textq2, $preanswer->q2_text);
			}


			$totalpre +=1;
			
			if ($preanswer->program != '')
				array_push($what_program, $preanswer->program);
			if ($preanswer->q3 != '')
				array_push($textq3, $preanswer->q3);
			if ($preanswer->q4 != '')
				array_push($textq4, $preanswer->q4);

		}

		foreach ($postanswers as $postanswer) {
			if ($postanswer->q5 == "A1") {
				$q5a1 += 1;
			}
			else if ($postanswer->q5 == "A2") {
				$q5a2 +=1;
			}
			else if ($postanswer->q5 == "A3") {
				$q5a3 +=1;
				array_push($textq5, $postanswer->q5_text);
			}			


			if ($postanswer->q6 == "A1") {
				$q6a1 += 1;
			}
			else if ($postanswer->q6 == "A2") {
				$q6a2 +=1;
			}
			else if ($postanswer->q6 == "A3") {
				$q6a3 +=1;
				array_push($textq6, $postanswer->q6_text);
			}
			
			$totalpost +=1;

			array_push($textq7, $postanswer->q7);
			if ($postanswer->q8 != '')
				array_push($textq8, $postanswer->q8);
			if ($postanswer->q9 != '')
				array_push($textq9, $postanswer->q9);
			if ($postanswer->q10 != '')
				array_push($textq10, $postanswer->q10);
			if ($postanswer->q11 != '')
				array_push($textq11, $postanswer->q11);

		}

		$data = array(
			array('Jag är inte intresserad av kursens innehåll och jag tänker inte lägga ner mer arbete än nödvändigt för att bli godkänd.', $q1a1, 'total number of answers', $totalpre),
			array('Jag vill lära mig det som krävs för att bli godkänd på kursen.', $q1a2, 'total number of answers', $totalpre),
			array('Jag vill lära mig mer än det som krävs för godkänt för att få ett högre betyg.', $q1a3, 'total number of answers', $totalpre),
			array('Jag vill lära mig mer än det som krävs för godkänt eftersom jag vill lära mig mer om ämnet och få en djupare förståelse för det kursen behandlar.', $q1a4, 'total number of answers', $totalpre),
			array('Annat.', $q1a5, 'total number of answers', $totalpre),
			array('Mitt mål är att få betyget 3 på kursen.', $q2a1, 'total number of answers', $totalpre),
			array('Mitt mål är att få betyget 4 på kursen.', $q2a2, 'total number of answers', $totalpre),
			array('Mitt mål är att få betyget 5 på kursen.', $q2a3, 'total number of answers', $totalpre),
			array('Annat.', $q2a4, 'total number of answers', $totalpre),
			array('Mina egna mål och min avsikt för kursen har förändrats under kursens gång.', $q5a1, 'total number of answers', $totalpost),
			array('Jag har samma mål och avsikt för kursen nu, som vid kursstart.', $q5a2, 'total number of answers', $totalpost),
			array('Annat.', $q5a3, 'total number of answers', $totalpost),
			array('Jag upplever att jag uppnått mina egna inlärningsmål.', $q6a1, 'total number of answers', $totalpost),
			array('Jag upplever att jag inte har uppnått mina egna inlärningsmål.', $q6a2, 'total number of answers', $totalpost),
			array('Annat.', $q6a3, 'total number of answers', $totalpost)
		);
		Excel::create('export', function($excel) use($data, $textq1, $textq2, $textq3, $textq4, $textq5, $textq6, $textq7, $textq8, $textq9, $textq10, $textq11, $what_program) {
			$excel->sheet('Answers', function($sheet) use($data) {				
				$sheet->fromArray($data);

			});
			$excel->sheet('Question 1, textanswers',function($sheet) use($textq1) {
				$sheet->fromArray($textq1);

			});
			$excel->sheet('Question 2, textanswers',function($sheet) use($textq2) {
				$sheet->fromArray($textq2);

			});
			$excel->sheet('Question 3, textanswers',function($sheet) use($textq3) {
				$sheet->fromArray($textq3);

			});
			$excel->sheet('Question 4, textanswers',function($sheet) use($textq4) {
				$sheet->fromArray($textq4);

			});
			$excel->sheet('Question 5, textanswers',function($sheet) use($textq5) {
				$sheet->fromArray($textq5);

			});
			$excel->sheet('Question 6, textanswers',function($sheet) use($textq6) {
				$sheet->fromArray($textq6);

			});
			$excel->sheet('Question 7, textanswers',function($sheet) use($textq7) {
				$sheet->fromArray($textq7);

			});
			$excel->sheet('Question 8, textanswers',function($sheet) use($textq8) {
				$sheet->fromArray($textq8);

			});
			$excel->sheet('Question 9, textanswers',function($sheet) use($textq9) {
				$sheet->fromArray($textq9);

			});
			$excel->sheet('Question 10, textanswers',function($sheet) use($textq10) {
				$sheet->fromArray($textq10);

			});
			$excel->sheet('Question 11, textanswers',function($sheet) use($textq11) {
				$sheet->fromArray($textq11);

			});
			$excel->sheet('Program',function($sheet) use($what_program) {
				$sheet->fromArray($what_program);

			});

		})-> export('xls');



	}

	/*--------------------------------------------------------------------------
	| doLogout
	|--------------------------------------------------------------------------
	| Logs out the current authenticated user.
	|--------------------------------------------------------------------------*/
	public function doLogout()
	{
		Auth::logout();
		return Redirect::route('adminLogin')
				->with('message', 'Du är nu utloggad');
	}
}

?>