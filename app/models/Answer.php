<?php

abstract class Answer extends Eloquent
{
	//Will return true if the table contains an answer from a specific user and survey.
	public static function hasAnswer($user_id, $survey_id)
	{
		$oldAnswer = self::where('user_id', '=', $user_id)
				->where('survey_id', '=', $survey_id)
				->get();
				
		return sizeof($oldAnswer)>0;
	}
	
	public function survey()
	{
		return $this->belongsTo('Survey');
	}
	
	public function user()
	{
		return $this->belongsTo('User');
	}
}