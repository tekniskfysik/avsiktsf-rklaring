@extends('layouts.default')

<?php $q1a1 = 0; ?>
<?php $q1a2 = 0; ?>
<?php $q1a3 = 0; ?>
<?php $q1a4 = 0; ?>
<?php $q1a5 = 0; ?>
<?php $q2a1 = 0; ?>
<?php $q2a2= 0; ?>
<?php $q2a3 = 0; ?>
<?php $q2a4 = 0; ?>
<?php $q5a1 = 0; ?>
<?php $q5a2 = 0; ?>
<?php $q5a3 = 0; ?>
<?php $q6a1 = 0; ?>
<?php $q6a2 = 0; ?>
<?php $q6a3 = 0; ?>

<?php $totalpre = 0; ?>
<?php $totalpost = 0; ?>



@section('content')
	<h3>Tillgängliga formulär</h3>
	<td>{{ $survey->courseName }}, {{ $survey->year }}, LP {{ $survey->LP }}</td><br><br>



	@foreach($preanswers as $preanswer)
		<tr>			
			{{-- <td>The user id is: {{ $preanswer->user_id }}, the survey id is: {{ $preanswer->survey_id}}, the preanswerid is: {{ $preanswer->id }}, the program is: {{ $preanswer->program }}, the answer to question 1 is: 
				{{ $preanswer->q1 }}, the answer to question 2 is: {{ $preanswer->q2 }}</td><br> --}}

				@if ($preanswer->q1 === "A1")
				<?php $q1a1 +=1; ?>
				@elseif ($preanswer->q1 === "A2")
				<?php $q1a2 +=1; ?>
				@elseif ($preanswer->q1 === "A3")
				<?php $q1a3 +=1; ?>
				@elseif ($preanswer->q1 === "A4")
				<?php $q1a4 +=1; ?>
				@elseif ($preanswer->q1 === "A5")
				<?php $q1a5 +=1; ?>
				@endif

				@if ($preanswer->q2 === "A1")
				<?php $q2a1 +=1; ?>
				@elseif ($preanswer->q2 === "A2")
				<?php $q2a2 +=1; ?>
				@elseif ($preanswer->q2 === "A3")
				<?php $q2a3 +=1; ?>
				@elseif ($preanswer->q2 === "A4")
				<?php $q2a4 +=1; ?>
				@endif

				<?php $totalpre +=1; ?>


		</tr>
	@endforeach

	@foreach($postanswers as $postanswer)
		<tr>			
			{{-- <td>The user id is: {{ $postanswer->user_id }}, the survey id is: {{ $postanswer->survey_id}}, the postanswerid is: {{ $postanswer->id }}, the program is: {{ $postanswer->program }}, the answer to question 1 is: 
				{{ $postanswer->q1 }}, the answer to question 2 is: {{ $postanswer->q2 }}</td><br> --}}


				@if ($postanswer->q5 === "A1")
				<?php $q5a1 +=1; ?>
				@elseif ($postanswer->q5 === "A2")
				<?php $q5a2 +=1; ?>
				@elseif ($postanswer->q5 === "A3")
				<?php $q5a3 +=1; ?>				
				@endif

				@if ($postanswer->q6 === "A1")
				<?php $q6a1 +=1; ?>
				@elseif ($postanswer->q6 === "A2")
				<?php $q6a2 +=1; ?>
				@elseif ($postanswer->q6 === "A3")
				<?php $q6a3 +=1; ?>				
				@endif

				<?php $totalpost +=1; ?>


		</tr>
	@endforeach

	<td> The pre-course questions have been answered by {{$totalpre}} students. </td><br>
	<td> The pre-course answers are listed below:</td><br><br>

	<td> On the first question the following answers were provided:  </td><br>
	<td> "Jag är inte intresserad av kursens innehåll och jag tänker inte lägga ner mer arbete än nödvändigt för att bli godkänd.", {{ $q1a1 }} / {{ $totalpre }}. </td><br>
	<td> "Jag vill lära mig det som krävs för att bli godkänd på kursen.", {{ $q1a2 }} / {{ $totalpre }}. </td><br>
	<td> "Jag vill lära mig mer än det som krävs för godkänt för att få ett högre betyg.", {{ $q1a3 }} / {{ $totalpre }}. </td><br>
	<td> "Jag vill lära mig mer än det som krävs för godkänt eftersom jag vill lära mig mer om ämnet och få en djupare förståelse för det kursen behandlar.", {{ $q1a4}} / {{ $totalpre }}. </td><br>
	<td> "Annat.", {{ $q1a5 }} / {{ $totalpre }}. </td><br><br>

	<td> On the second question the following answers were provided:  </td><br>
	<td> "Mitt mål är att få betyget 3 på kursen.", {{ $q2a1 }} / {{ $totalpre }}. </td><br>
	<td> "Mitt mål är att få betyget 4 på kursen.", {{ $q2a2 }} / {{ $totalpre }}. </td><br>
	<td> "Mitt mål är att få betyget 5 på kursen.", {{ $q2a3 }} / {{ $totalpre }}. </td><br>
	<td> "Annat.", {{ $q2a4 }} / {{ $totalpre }}. </td><br><br>

	<td> The post-course questions have been answered by {{$totalpost}} students. </td><br>
	<td> The post-course answers are listed below:</td><br><br>

	<td> On the first question the following answers were provided:  </td><br>
	<td> "Mina egna mål och min avsikt för kursen har förändrats under kursens gång.", {{ $q5a1 }} / {{ $totalpost }}. </td><br>
	<td> "Jag har samma mål och avsikt för kursen nu, som vid kursstart.", {{ $q5a2 }} / {{ $totalpost }}. </td><br>	
	<td> "Annat.", {{ $q5a3 }} / {{ $totalpost }}. </td><br><br>

	<td> On the second question the following answers were provided:  </td><br>
	<td> "Jag upplever att jag uppnått mina egna inlärningsmål.", {{ $q6a1 }} / {{ $totalpost }}. </td><br>
	<td> "Jag upplever att jag inte har uppnått mina egna inlärningsmål.", {{ $q6a2 }} / {{ $totalpost }}. </td><br>	
	<td> "Annat.", {{ $q6a3 }} / {{ $totalpost }}. </td><br><br>

	<td> The written answers can be viewed in the downloadable excelfile below. </td><br><br>

	{{ link_to_route('adminSurveyInfo', 'Tillbaka', $parameters=array('id'=>$survey->id)) }} <br>

	{{ link_to_route('exportExcel', 'ExportExcel', $parameters=array('id'=>$survey->id)) }}

@stop