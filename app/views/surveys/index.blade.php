@extends('layouts.default')

@section('content')
	<h3>Tillgängliga formulär</h3>
	<table>
	@foreach($surveys as $survey)
		<tr>
			<td>{{ $survey->courseName }}, {{ $survey->year }}, LP {{ $survey->LP }}</td>
			<td>{{ link_to_route('login', 'logga in', $parameters=array($survey->id)) }}</td>
		</tr>
	@endforeach
	</table>
@stop