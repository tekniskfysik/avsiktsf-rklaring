<?php

class StudentController extends BaseController
{
	/*--------------------------------------------------------------------------
	| index
	|--------------------------------------------------------------------------
	| Returns the index page for each course and student.  
	|--------------------------------------------------------------------------*/
	public function get_index()
	{	
		$user_id = Auth::user()->id;
		$survey_id = Auth::user()->survey_id;
		$hasAnsweredPreForm = Preanswer::hasAnswer($user_id, $survey_id);
		$hasAnsweredPostForm = Postanswer::hasAnswer($user_id, $survey_id);
		
		return View::make('student.index')
				->with('title', 'Home page')
				->with('hasAnsweredPreForm', $hasAnsweredPreForm)
				->with('hasAnsweredPostForm', $hasAnsweredPostForm)
				->with('survey', Auth::user()->survey);
	}

	/*--------------------------------------------------------------------------
	| preForm
	|--------------------------------------------------------------------------
	| Returns the page with all questions that should be answered in the
	| beginning of the course.
	| Every question is passed in as a parameter, so is the alternatives in 
	| the radio questions.
	|--------------------------------------------------------------------------*/
	public function get_preForm()
	{	
		$a1 = Alternative::where('question_code', 'q1')->get();
		$a2 = Alternative::where('question_code', 'q2')->get();
		return View::make('student.preForm')
				->with('title', 'Frågor vid kursstart')
				->with('user_id', Auth::user()->id)
				->with('survey', Auth::user()->survey)
				->with('q0', Question::where('code', 'program')->first())
				->with('q1', Question::where('code', 'q1')->first())
				->with('a1', $a1)
				->with('q2', Question::where('code', 'q2')->first())
				->with('a2', $a2)
				->with('q3', Question::where('code', 'q3')->first())
				->with('q4', Question::where('code', 'q4')->first());
	}
	
	/*--------------------------------------------------------------------------
	| preForm
	|--------------------------------------------------------------------------
	| Posts the answers to the preanswers-table. The function checks if the 
	| table already contains an answer to that survey from the logged in user
	| before the actual post. After the post the user is redirected to the index page.
	|--------------------------------------------------------------------------*/
	public function post_preForm()
	{
		if( Preanswer::hasAnswer(Auth::user()->id, Auth::user()->survey_id) )
		{
			return Redirect::route('studentHome')
				->with('message', 'Du har redan svarat på detta formulär');
		}
		
		else
		{
			$validation = Preanswer::validate(Input::all());
			
			if($validation->fails())
			{
				return Redirect::back()
						->withErrors($validation)
						->withInput();
			}
			
			else
			{
				$ans = new Preanswer;
				$ans->program = Str::lower(Input::get('program'));
				$ans->survey_id = Input::get('survey_id');
				$ans->user_id = Input::get('user_id');
				$ans->q1 = Input::get('q1');
				$ans->q1_text = Input::get('q1_text');
				$ans->q2 = Input::get('q2');
				$ans->q2_text = Input::get('q2_text');
				$ans->q3 = Input::get('q3');
				$ans->q4 = Input::get('q4');
				$ans->save();

				return Redirect::route('studentHome')
					->with('message', 'Svaren är mottagna');
			}
		}
	}
	
	/*--------------------------------------------------------------------------
	| postForm
	|--------------------------------------------------------------------------
	| Returns the page with all questions that should be answered in the
	| end of the course.
	|--------------------------------------------------------------------------*/
	public function get_postForm()
	{
		$a5 = Alternative::where('question_code', 'q5')->get();
		$a6 = Alternative::where('question_code', 'q6')->get();
		
		return View::make('student.postForm')
				->with('title', 'Frågor vid kursslut')
				->with('user_id', Auth::user()->id)
				->with('survey', Auth::user()->survey)
				->with('q5', Question::where('code', 'q5')->first())
				->with('a5', $a5)
				->with('q6', Question::where('code', 'q6')->first())
				->with('a6', $a6)
				->with('q7', Question::where('code', 'q7')->first())
				->with('q8', Question::where('code', 'q8')->first())
				->with('q9', Question::where('code', 'q9')->first())
				->with('q10', Question::where('code', 'q10')->first())
				->with('q11', Question::where('code', 'q11')->first());
	}
	
	/*--------------------------------------------------------------------------
	| postForm
	|--------------------------------------------------------------------------
	| Posts the answers to the postanswers-table. The function checks if the 
	| table already contains an answer to that survey from the logged in user
	| before the actual post. After the post the user is redirected to the index page.
	|--------------------------------------------------------------------------*/
	public function post_postForm()
	{
		if( Postanswer::hasAnswer(Auth::user()->id, Auth::user()->survey_id) )
		{
			return Redirect::route('studentHome')
				->with('message', 'Du har redan svarat på detta formulär');
		}
		
		else
		{
			$validation = Postanswer::validate(Input::all());
			
			if($validation->fails())
			{
				return Redirect::back()
						->withErrors($validation)
						->withInput();
			}
			
			else
			{
				$ans = new Postanswer;
				$ans->survey_id = Input::get('survey_id');
				$ans->user_id = Input::get('user_id');
				$ans->q5 = Input::get('q5');
				$ans->q5_text = Input::get('q5_text');
				$ans->q6 = Input::get('q6');
				$ans->q6_text = Input::get('q6_text');
				$ans->q7 = Input::get('q7');
				$ans->q8 = Input::get('q8');
				$ans->q9 = Input::get('q9');
				$ans->q10 = Input::get('q10');
				$ans->q11 = Input::get('q11');
				$ans->save();

				return Redirect::route('studentHome')
					->with('message', 'Svaren är mottagna');
			}
		}
	}
	/*--------------------------------------------------------------------------
	| preAnswers
	|--------------------------------------------------------------------------
	| Makes it possible for the current user to see how he/she answered
	| the pre-form after it is answered.
	|--------------------------------------------------------------------------*/

	public function get_preAnswers()
	{


	$preanswer = Preanswer::where('user_id', '=', Auth::user()->id)
	->where('survey_id', '=', Auth::user()->survey_id)
	->first();

	return View::make('student.userPreAnswers')
	->with('title', 'Student Answers')
	->with('survey', Auth::user()->survey)
	->with('preanswer', $preanswer);
	}
}

?>