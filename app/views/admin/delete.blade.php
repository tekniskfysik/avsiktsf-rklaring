@extends('layouts.default')

@section('content')

	<p>Är du säker på att du vill radera formuläret till</p>
	<h2>{{ $survey->courseName }}, LP {{ $survey->LP }}, {{ $survey->year }} </h2>
	<p>samt alla svar? Notera att informationen blir permanent raderad och bör därför exporteras till en excel-fil först.</p>

	{{ Form::open(array('route'=>array('adminDoDelete', $survey->id), 'method'=>'delete')) }}
		{{ Form::hidden('survey_id', $survey['id']) }}
		{{ Form::submit('Radera') }}
	{{ Form::close() }}
	
	<br />
	{{ link_to_route('adminSurveyList', 'Avbryt') }}
	
@stop