<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreanswersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('preanswers', function(Blueprint $table)
		{
			$table->increments('id');
			
			$table->string('program')->nullable();
			$table->string('q1')->nullable();
			$table->string('q1_text')->nullable();
			$table->string('q2')->nullable();
			$table->string('q2_text')->nullable();
			$table->string('q3')->nullable();
			$table->string('q4')->nullable();
			$table->integer('survey_id');
			$table->integer('user_id');
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('preanswers');
	}

}
