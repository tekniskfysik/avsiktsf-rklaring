<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class UsersController extends BaseController
{
	/*--------------------------------------------------------------------------
	| showRegister
	|--------------------------------------------------------------------------
	| Returns the view with the registration page. The function first checks if
	| the survey exists, if not, an error page will be returned.
	|--------------------------------------------------------------------------*/
	public function get_showRegister($id)
	{
		try {
			$survey = Survey::findOrFail($id);
		}
		catch(ModelNotFoundException $e) {
			return Response::make('Not Found', 404);
		}
		return View::make('surveys.register')
				->with('title', 'Registrering')
				->with('survey', $survey);
	}
	
	/*--------------------------------------------------------------------------
	| doRegister
	|--------------------------------------------------------------------------
	| Validates a user's information and register him/her if the validation 
	| passes.
	|--------------------------------------------------------------------------*/
	public function post_doRegister()
	{
		$input = Input::all();
		
		$validation = User::validate(Input::all());
		
		if($validation->fails())
		{
			return Redirect::back()
				->withErrors($validation)
				->withInput();
		}
		else
		{
			if(User::userExist(Input::get('email'), Input::get('survey_id')))
			{
				return Redirect::back()
						->with('message', 'Den angivna e-postadressen är redan registrerad på den här kursen');
			}
			else
			{
				$user = new User;
				$user->email = Input::get('email');
				$user->password = Hash::make(Input::get('password'));
				$user->survey_id = Input::get('survey_id');
				$user->role = Input::get('role');
				$user->save();
				
				Auth::login($user);
				
				return Redirect::route('studentHome');
			}
		}
	}

	/*--------------------------------------------------------------------------
	| showLogin
	|--------------------------------------------------------------------------
	| Returns the view with the login page. The function first checks if
	| the survey exists, if not, an error page will be returned.
	|--------------------------------------------------------------------------*/
	public function get_showLogin($id)
	{
		try {
			$survey = Survey::findOrFail($id);
		}
		catch(ModelNotFoundException $e) {
			return Response::make('Not Found', 404);
		}
		return View::make('surveys.login')
				->with('title', 'Login')
				->with('survey', $survey);
	}

	/*--------------------------------------------------------------------------
	| doLogin
	|--------------------------------------------------------------------------
	| Logs in and redirects a user to the student index page. If the authentication
	| fails the user will be redirected to back the login page.
	|--------------------------------------------------------------------------*/
	public function post_doLogin()
	{
		$input = Input::all();
		
		$attempt = Auth::attempt(array(
					'email' => $input['email'],
					'password' => $input['password'],
					'survey_id' => $input['id']
		));
		
		if($attempt)
		{
			return Redirect::route('studentHome');
		}
		else 
		{
			return Redirect::back()
					->withInput()
					->with('message', 'Felaktiga inloggningsuppgifter');
		}
	}

	/*--------------------------------------------------------------------------
	| doLogout
	|--------------------------------------------------------------------------
	| Logs out the current authenticate user.
	|--------------------------------------------------------------------------*/
	public function doLogout()
	{
		$id = Auth::user()->survey_id;
		Auth::logout();
		return Redirect::route('login', array('id' => $id))
				->with('message', 'Du är nu utloggad');
	}
}