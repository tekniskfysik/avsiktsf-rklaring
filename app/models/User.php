<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	
	protected $fillable = array('email');
	protected $guarded = array('password', 'id', 'role', 'survey_id');
	
	public static $rules = array(
		'email' => 'required|email',
		'password' => 'required|min:4|confirmed',
		'password_confirmation' => 'required|min:4'
	);
	
	public static $messages = array(
		'email.required' => 'Du måste ange en e-postadress',
		'password.required' => 'Du måste ange ett lösenord',
		'password_confirmation.required' => 'Du måste upprepa lösenordet',
		'email.email' => 'Ej godkänd e-postadress',
		'password.min' => 'Lösenordet måste vara minst 4 tecken',
		'password_confirmation.min' => 'Det uppreade lösenordet måsta vara minst 4 tecken',
		'password.confirmed' => 'Lösenorden är inte identiska'
	);
	
	public static function validate($data)
	{
		return Validator::make($data, static::$rules, static::$messages);
	}
	
	//Returns true if the user exists in the table.
	// ToDo?? Make this case-insensitive?
	public static function userExist($email, $survey_id)
	{
		$exist = self::where('email', '=', $email)
				->where('survey_id', '=', $survey_id)
				->get();
				
		return sizeof($exist)>0;
	}
	
	public function survey()
	{
		return $this->belongsTo('Survey');
	}
	
	public function preanswer()
	{
		return $this->hasOne('Preanswer');
	}
	
	public function postanswer()
	{
		return $this->hasOne('Postanswer');
	}
}
