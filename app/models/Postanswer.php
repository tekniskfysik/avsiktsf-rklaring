<?php

class Postanswer extends Answer
{
	protected $table = 'postanswers';
	
	//Only these attributes are able to be filled
	protected $fillable = array('q5',
								'q5_text',
								'q6',
								'q6_text',
								'q7',
								'q8',
								'q9',
								'q10',
								'q11');
								
	protected $guarded = array('id', 'user_id', 'survey_id');
	
	public static $rules = array(
		'q5' => 'required',
		'q6' => 'required'
	);
	
	public static $messages = array(
		'q5.required' => 'Du måste ange ett alternativ på fråga 1',
		'q6.required' => 'Du måste ange ett alternatvi på fråga 2',
		'q5_text.required' => 'Var vänlig fyll i textfältet till fråga 1',
		'q6_text.required' => 'Var vänlig fyll i textfältet till fråga 2'
	);
	
	//If the user chooses "annat: ", i.e. A3, the text field is required.
	public static function validate($data)
	{
		$v = Validator::make($data, static::$rules, static::$messages);
		
		$v->sometimes('q5_text', 'required', function($data){
			return str_is('A3', $data->q5);
		});
		
		$v->sometimes('q6_text', 'required', function($data){
			return str_is('A3', $data->q6);
		});
		
		return $v;
	}
}

?>