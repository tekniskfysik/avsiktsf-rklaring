<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostanswersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('postanswers', function(Blueprint $table)
		{
			$table->increments('id');
			
			$table->string('q5')->nullable();
			$table->string('q5_text')->nullable();
			$table->string('q6')->nullable();
			$table->string('q6_text')->nullable();
			$table->string('q7')->nullable();
			$table->string('q8')->nullable();
			$table->string('q9')->nullable();
			$table->string('q10')->nullable();
			$table->string('q11')->nullable();
			$table->integer('survey_id');
			$table->integer('user_id');			
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('postanswers');
	}

}
