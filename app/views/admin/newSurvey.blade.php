@extends('layouts.default')

@section('content')
	
	<h1>Skapa nytt formulär</h1>
	
	{{ Form::open(array('route'=>'adminStoreSurvey', 'method'=>'post')) }}
	<table>
		<tr>
			<td>{{ Form::label('courseName', 'Kursnamn:') }}</td>
			<td>{{ Form::text('courseName', Input::old('courseName')) }}</td>
		</tr>
		<tr>
			<td>{{ Form::label('year', 'År:') }}</td>
			<td>{{ Form::text('year', Input::old('year')) }}</td>
		</tr>
		<tr>
			<td>{{ Form::label('LP', 'Läsperiod:') }}</td>
			<td>{{ Form::selectRange('LP', 1, 4) }}</td>
		</tr>
		<tr>
			<td>{{ Form::submit('Skapa') }}</td>
			<td></td>
		</tr>
	</table>
	{{ Form::close() }}
	
	{{ link_to_route('admin', 'Tillbaka') }}
	
@stop