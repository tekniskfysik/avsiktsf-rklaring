@extends('layouts.default')

@section('content')

	@if (Session::has('error'))
		<span style="color:red">
			Vi kan inte hitta en användare med den angivna e-postadressen
		</span>
	@elseif (Session::has('success'))
		<span style="color:green">
			Ett e-postmeddelande har skickats till den angivna e-postadressen
		</span>
	@endif

	<h2>{{ $survey['courseName'] }}, LP {{ $survey['LP'] }}, {{ $survey['year'] }}</h2>
	<p>Har du glömt ditt lösenord till kursen?</p>
	<p>Fyll i din e-postadress nedan, du kommer att få ett mail som hjälper dig att skapa ett nytt lösenord.</p>
	
	{{ Form::open(array('route'=>array('postRemind', $survey->id), 'method'=>'post')) }}
	<table>
		<tr>
			<td>{{ Form::label('email', 'E-mail:') }}</td>
			<td>{{ Form::text('email', Input::old('email')) }}</td>
		</tr>
		{{ Form::hidden('survey_id', $survey['id']) }}
		<tr>
			<td>{{ Form::submit('Skicka') }}</td>
			<td></td>
		</tr>
	</table>
	{{ Form::close() }}
	
	<br />
	{{ link_to_route('login', 'Tillbaka', $parameters=array('id'=>$survey['id'])) }}
@stop